import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('../../course/data/ERP.csv', delimiter=',')

fs=500
selected = data[:10, :fs*2]
t = np.linspace(0, 2, 2*fs, endpoint=False)

# Version with overlap

plt.plot(t, selected.T)
plt.show()


# Version without overlap

# Use std as estimate of signal separation
space = selected.std(axis=1).max() * 3
for i in range(selected.shape[0]):
    plt.plot(t, selected[i,:] + i*space)
plt.show()
