import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('../../course/data/ERP.csv', delimiter=',')

fs=500
t = np.linspace(0, 2, 2*fs, endpoint=False)
plt.plot(t, data[:,:fs*2].mean(axis=0))
plt.show()


# Version with all time series
plt.plot(t, data[:,:fs*2].T, color='blue', alpha=0.2)
plt.plot(t, data[:,:fs*2].mean(axis=0), color='red', linewidth=2)
plt.xlabel('Time (s)')
plt.title('Event Related Potential')
plt.show()