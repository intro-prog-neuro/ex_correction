import numpy as np
import matplotlib.pyplot as plt
import nibabel as ni

image  = ni.load('../../course/data/someones_anatomy.nii.gz')
data = image.get_fdata()  # get the numpy array

for i in range(data.shape[0]):
    plt.imshow(data[i,:,:].T, origin='lower', cmap='gray')
    plt.savefig('/tmp/slice{}.png'.format(i))
    plt.show()