#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 08:45:08 2024

@author: chanel
"""

def concatenate(in1, in2, out):
    with open(in1, 'r') as if1:
        lines1 = if1.readlines()
    with open(in2, 'r') as if2:
        lines2 = if2.readlines()

    with open(out, 'w') as of:
        for i in range(len(lines1)):
            of.write(lines1[i].strip() + ' ' + lines2[i])

concatenate('/tmp/statements.txt', '/tmp/names.txt', '/tmp/full.txt')