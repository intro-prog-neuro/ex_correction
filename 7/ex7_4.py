#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 09:39:53 2024

@author: chanel
"""
import statistics as s


iAge = 6

def readages(filename):
    ages = []
    with open(filename, 'r') as f:
        f.readline()
        for line in f:
            participant = line.strip().split(',')
            ages.append(int(participant[iAge]))
    return ages

# Implementing your own functions
def mean(l):
    acc = 0
    for e in l:
        acc += e
    return acc / len(l)

def std(l, mu=None):
    acc = 0
    if mu == None:
        mu = mean(l)
    for e in l:
        acc += (e - mu) ** 2
    return (acc / (len(l)-1)) ** (1/2)
    
# Easy way
def stats(l):
    return s.mean(l), s.stdev(l)    

ages = readages('participants-100.csv')

# Comparing / using both methods
print(mean(ages), std(ages))
print(stats(ages))