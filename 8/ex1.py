import csv

def read_participants(filename):
    participants = {}
    with open(filename, newline='') as f:
        reader = csv.reader(f)
        titles = next(reader)  # Get the first special row
        for t in titles:
            participants[t] = []
        for row in reader:
            for i, t in enumerate(titles):
                participants[t].append(row[i])
    return participants


def read_participants2(filename):
    participants = {}
    with open(filename, newline='') as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            for title in row:
                if title not in participants:
                    participants[title] = [row[title]]
                else:
                    participants[title].append(row[title])
    return participants


# With this weird if statement this code will be executed only if the script
# is executed by a python interpreter
# For instance if this file/module is imported the code below will
# NOT be executed
if __name__ == "__main__":
    #p = read_participants('../../course/data/participants-100.csv')
    p = read_participants2('../../course/data/participants-100.csv')
    print(p)
    print(p['Id'])
    print(p['Hand'])




