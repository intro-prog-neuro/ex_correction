import csv
from ex1 import read_participants

def select(sigs, elec_names):
    D = []
    for k in sigs:
        if k in elec_names:
            D.append(sigs[k])
    return D


def select2(sigs, elec_names):
    return [sigs[k] for k in sigs if k in elec_names]

signals = read_participants('../../course/data/EEG_blink.csv')

selected_sigs = select(signals,  ['T7', 'F4','O2' ])
selected_sigs2 = select2(signals,  ['T7', 'F4','O2' ])

# Compare both
same = True
for i,_ in enumerate(selected_sigs):
    for j,_ in enumerate(selected_sigs[i]):
        if selected_sigs[i][j] != selected_sigs2[i][j]:
            same = False

if not same:
    print('Lists are diffrent !')