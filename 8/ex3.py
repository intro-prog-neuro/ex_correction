import csv
import statistics as stat
from ex1 import read_participants

# This function takes a function as paramter
# This allows to apply any function we want to the data
# in the dictionary
# For instance func=stat.mean or func=stat.stdev or any
# function taking a list as parameter as input
def compute(dictionary, func=stat.mean):
    return {k: func(dictionary[k]) for k in dictionary}


signals = read_participants('../../course/data/EEG_blink.csv')

# Convert all lists content to floats (should be done in previous function ?)
for k in signals:
    signals[k] = [float(v) for v in signals[k]]

print(compute(signals, stat.mean))
print(compute(signals, stat.stdev))