import csv
import statistics as stat
from ex1 import read_participants

def compute_mean(dictionary):
    means = {}
    for k in dictionary:
        means[k] = stat.mean(dictionary[k])
    return means

# Same but with dict comprehension
def compute_mean2(dictionary):
    return {k: stat.mean(dictionary[k]) for k in dictionary}


signals = read_participants('../../course/data/EEG_blink.csv')


# Convert all lists content to floats (should be done in previous function ?)
for k in signals:
    signals[k] = [float(v) for v in signals[k]]
    
print(compute_mean(signals))