#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 09:41:15 2024

@author: chanel
"""

def average(s1, s2):
    avgs = [0] * len(s1)
    for i, v1 in enumerate(s1):
        avgs[i] = (v1 + s2[i]) / 2
    return avgs