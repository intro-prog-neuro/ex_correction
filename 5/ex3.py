#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 08:51:29 2024

@author: chanel
"""

def min_max(signal):
    smallest = biggest = signal[0]
    for s in signal:
        if s < smallest:
            smallest = s
        if s > biggest:
            biggest = s
    return smallest, biggest            