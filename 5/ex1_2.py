#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 08:08:28 2024

@author: chanel
"""

x = 10
y = 20

def exchange(x, y): 
    temp = x
    x = y
    y = temp
    return x, y


def exchange2(x, y): 
    return y, x

(y,x) = (x,y)