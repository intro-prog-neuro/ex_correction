import csv

def read_csv(filename, **csvreader_args):
    """    
        This function reads a CSV file with the first line as header. It also attempts to
        convert numeric data to floats.
        
        Args:
            filename: the name of the file
            csvreader_args: arguments passed to the csv.reader python function
        
        Returns: a dictionnary with each column name as keys and and columns values as lists.
    """
    data = {}
    with open(filename) as f:
        # Read all data
        reader = csv.reader(f, csvreader_args)
        titles = next(reader)  # Get the first special row
        for t in titles:
            data[t] = []
        for row in reader:
            for i, t in enumerate(titles):
                data[t].append(row[i])

    # Attempt to convert numeric columns
    # Note: the is numeric funnction only check if a string contains digits.
    # To determine if a value like '-1.352' is numeric, the code below removes
    # the first occurences of '.' and '-' and than check if only digits remains
    for t in titles:      
        isnum =  all([s.replace('.','',1).replace('-','',1).isnumeric() for s in data[t]])  # !! Bug if string contains digits with '-' in the middle
        if isnum:
            data[t] = [float(s) for s in data[t]]

    return data