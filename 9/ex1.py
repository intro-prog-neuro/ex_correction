import readers
import statistics as stat

data = readers.read_csv('../../course/data/participants-100.csv')

# Get the date of the first experiment
print('Experiment started on: ' + sorted(data['Experiment date'])[0])

def apply_group(group_lst, func=None, apply_lst=None, groups=None):
    """This function returns a dictionnary which groups data by unique values,
    and can apply functions on these grouped values.

    Args:
        group_lst (list): the list used to form groups
        func (function, optional): the function to apply on each group. If None
        the values associated to each groups are direcetly returned. Defaults to None.
        apply_lst (list, optional): the values processed for each group, must be the
        same length as group_lst. If None group_lst is used as values. Defaults to None.
        groups (set, optional): a set of values defining the groups. If None the set
        is composed of unique values of group_lst. Defaults to None.

    Returns:
        dict: a dictionnary indexed by each groups value. The content fo the dictionnary
        at each index is the result of func applied on the group values
        (or the values if func is None).
    """
    if groups == None:
        groups = set(group_lst)
    if apply_lst == None:
        apply_lst = group_lst
    result = {}
    for c in groups:
        values = [v for i,v in enumerate(apply_lst) if  group_lst[i] == c]
        if func is not None:
            values = func(values)
        result[c] = values
    return result

# Number of participants per condition (all conditions considered)
print('Number of participants per condition: ', apply_group(data['Condition'], len))

# Number of participants per handness (all conditions considered)
print('Number of participants per handness: ', apply_group(data['Hand'], len))

# Mean age of participants per gender (all conditions considered)
print('Mean age of participants per gender: ', apply_group(data['Gender'], stat.mean, apply_lst=data['Age']))

# Mean age of participants per gender (only males and females)
print('Mean age of participants per gender (only M/F): ', apply_group(data['Gender'], stat.mean, apply_lst=data['Age'], groups=['M', 'F']))