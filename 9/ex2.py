import readers
import statistics as stat
import scipy.stats as sstat
from ex1 import apply_group

data = readers.read_csv('../../course/data/EEG_blink.csv')

# Select only EEG columns
EEG = [k for k in data if len(k) < 4]

# Compute standard deviation for each column
std = {k: stat.stdev(data[k]) for k in EEG}

# Select electrodes
thresh = 30
print('Bad electrodes: ', [k for k in std if std[k] > thresh])

# select signals
for e in EEG:
    print('Processing electrode ' + e)
    print('mean: ', apply_group(data['eyeDetection'], func=stat.mean, apply_lst=data[e]))
    print('std: ', apply_group(data['eyeDetection'], func=stat.stdev, apply_lst=data[e]))
    grp = apply_group(data['eyeDetection'], apply_lst=data[e])
    print('ttest:', sstat.ttest_ind(grp[0], grp[1]))