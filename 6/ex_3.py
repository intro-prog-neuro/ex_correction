#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 09:27:58 2024

@author: chanel
"""

a = [1,2,3]    #-> creation of a new object 
a = a + [4]    #-> creation of a new object
a += [4]       #-> modification in place
a.append('z')  #-> modification in place
a[1] = 3       #-> modification in place
a = (1,2,3)    #-> creation of new tuple
a = a + (4,)   #-> creation of new tuple
a += (4,)      #-> creation of new tuple
