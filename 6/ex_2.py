#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 09:03:53 2024

@author: chanel
"""

L = ['I', 'am', 'happy', 'with', 'my', 'cat', '.', 'It', 'is', 'a', 'good', 'cat', 'and', 'I', 'love', 'it', 'so', 'much', 'that', 'I', 'would', 'like', 'to', 'call', 'it', 'a', 'Cat', '.', 'Some', 'people', 'call', 'it', 'CaT', 'but', 'this', 'is', 'too', 'much,', 'a', 'cat', 'is', 'not', 'at', 'the', 'level', 'of', 'a', 'dog', '.', 'But', 'still', 'a', 'cat', 'is', 'nice,', 'or', 'maybe', 'it', 'is', 'not', 'in', 'the', 'end', '.']

key = 'cat'
stop = 'dog'

def count_keyword(words, keyword, stopword=''):
    count = 0
    i = 0
    while i < len(words) and words[i].lower() != stopword:
        if words[i].lower() == keyword:
            count = count + 1
        i = i + 1
    return count    
        
print(count_keyword(L, key, stop))
print(count_keyword(L, key))
print(count_keyword(L, key, '.'))
print(count_keyword(L, 'happy', '.'))