#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 08:20:47 2024

@author: chanel
"""

L = ['P_1', 'p_1', 'Part_5', 'participant_7', 'P_8', 'P_9']


# Solution for

P = []
for s in L:
    p_int = int(s.split('_')[-1])
    P.append(p_int)

# Solution while

P = []
i = 0
while i < len(L):
    p_int = int(L[i].split('_')[-1])
    P.append(p_int)
    i = i + 1

# Solution while
# Warning: this empties the list -> inplace operation
P = []
while len(L) > 0:
    s = L.pop(0)
    p_int = int(s.split('_')[-1])
    P.append(p_int)