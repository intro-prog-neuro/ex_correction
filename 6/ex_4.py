#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 09:52:31 2024

@author: chanel
"""

def clean_copy(l, e):
    new_l = []
    for v in l:
        if v != e:
            new_l.append(v)
    return new_l


# For this function a while loop is used because a for loop on a range would not work
# The reason is that the length of list changes if the pop function is used
# with the while loop to new length is checked at each iteration
def clean_inline(l, e):
    i = 0
    while i < len(l):
        if l[i] == e:
            l.pop(i)
        i = i + 1
    # The line below is not needed as l was modified by the function
    # try to remove it and check the value of the list after being given to the function:
    # l = [1,2,3,1,3,2]
    # clean_inline(l, 3)  # you can also try with clean_copy to see the difference
    # l
    return l