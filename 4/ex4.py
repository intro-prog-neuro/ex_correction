#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 09:41:15 2024

@author: chanel
"""

def average(s1, s2):
    avgs = []
    for i in range(len(s1)):
        avgs = avgs + [(s1[i] + s2[i]) / 2]
    return avgs


def average_bi(s1, s2):
    avgs = [0] * len(s1)
    for i in range(len(s1)):
        avgs[i] = (s1[i] + s2[i]) / 2
    return avgs


print(average([1,2,3],[2,3,4]))