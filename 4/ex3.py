#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 14:18:32 2024

@author: chanel
"""

def minimum(signal):
    mini = signal[0]
    for s in signal:
        if s < mini:
            mini = s
    return mini


def maximum(signal):
    maxi = signal[0]
    for s in signal:
        if s > maxi:
            maxi = s
    return maxi