#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 09:19:48 2024

@author: chanel
"""

def count1(electrodes):
    count = 0
    for e in electrodes:
        if e == 'F1':
            count = count + 1
    return count


def count2(electrodes, target_elec):
    count = 0
    for e in electrodes:
        if e == target_elec:
            count = count + 1
    return count