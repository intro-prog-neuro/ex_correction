#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 14:25:29 2024

@author: chanel
"""

def get_occipital(list_elec):
    list_occ = []
    for e in list_elec:
        if e[0] == 'O':
            list_occ = list_occ + [e]
    return list_occ