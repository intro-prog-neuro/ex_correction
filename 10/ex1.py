import numpy as np
import scipy.stats as stats

def times(mean, std):
    return np.random.randn(30,1)*std + mean

# Use one or the other...
data = np.loadtxt('../../course/data/go-nogo.csv', delimiter=',')
# data = np.concatenate((times(500, 35), times(530,35)), axis=1)

ttr = stats.ttest_rel(data[:,0], data[:,1])

if ttr.pvalue < 0.05:
    m = data.mean(axis=0)
    std = data.std(axis=0)
    print("Test is significant (t={:.2f}, p={:.5f})".format(ttr.statistic, ttr.pvalue))
    print("Go: \u03BC={:.2f}, \u03C3={:.2f}; NoGo: \u03BC={:.2f}, \u03C3={:.2f}".format(m[0], std[0], m[1], std[1]))
else:
    print("Test was NOT significant (t={:.0f}, p={:.0f})".format(ttr.statistic, ttr.pvalue))