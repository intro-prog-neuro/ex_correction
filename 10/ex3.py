import numpy as np
import nibabel as ni

image  = ni.load('../../course/data/someones_anatomy.nii.gz')
data = image.get_fdata()  # get the numpy array


# Assumes that values > 10 is part of the brain
thresh = 10
thresh_img = data > thresh

# summing boolean values (True -> 1, False -> 0)
nb_brain = thresh_img.sum(axis=(1,2))

# As we would do before knowing about numpy and other functions:
imax = 0
vmax = 0
for i in range(nb_brain.shape[0]):
    if nb_brain[i] > vmax:
        vmax = nb_brain[i]
        imax = i
print('The {}th image is the one with most brain in it'.format(imax))

# But now:
print('The {}th image is the one with most brain in it'.format(nb_brain.argmax()))