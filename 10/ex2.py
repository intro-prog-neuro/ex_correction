import numpy as np

def select(data, threshold):
    return data[data.std(axis=1) < threshold,:]

data = np.loadtxt('../../course/data/ERP.csv', delimiter=',')
selected = select(data, 1)